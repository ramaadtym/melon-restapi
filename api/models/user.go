package models
import (
    "errors"
    "strings"
    "github.com/jinzhu/gorm"
    "golang.org/x/crypto/bcrypt"
)
// Users model
type Users struct {
    gorm.Model
    Username     string `gorm:"type:varchar(255)" 			   json:"username"`
    Name    	 string `gorm:"size:100;not null"              json:"name"`
    Password     string `gorm:"size:100;not null"              json:"password"`
    Div          string `gorm:"size:100;not null"              json:"div"`
    Issuperadmin bool `gorm:"false"                       	   json:"is_superadmin"`
}
// HashPassword hashes Password from user input
func HashPassword(Password string) (string, error) {
    bytes, err := bcrypt.GenerateFromPassword([]byte(Password), 14) // 14 is the cost for hashing the Password.
    return string(bytes), err
}

// CheckPasswordHash checks Password hash and Password from user input if they match
func CheckPasswordHash(Password, hash string) error {
    err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(Password))
    if err != nil {
        return errors.New("Password incorrect")
    }
    return nil
}

// BeforeSave hashes user Password
func (u *Users) BeforeSave() error {
    Password := strings.TrimSpace(u.Password)
    hashedPassword, err := HashPassword(Password)
    if err != nil {
        return err
    }
    u.Password = string(hashedPassword)
    return nil
}

// Prepare strips user input of any white spaces
func (u *Users) Prepare() {
    u.Username = strings.TrimSpace(u.Username)
    u.Name = strings.TrimSpace(u.Name)
}

// Validate user input
func (u *Users) Validate(action string) error {
    switch strings.ToLower(action) {
    case "login":
        if u.Username == "" {
            return errors.New("Username is required")
        }
        if u.Password == "" {
            return errors.New("Password is required")
        }
        return nil
    default: // this is for creating a user, where all fields are required
        if u.Name == "" {
            return errors.New("name is required")
        }
        if u.Username == "" {
            return errors.New("Username is required")
        }
        if u.Password == "" {
            return errors.New("Password is required")
        }
        return nil
    }
}

// SaveUsers adds a user to the database
func (u *Users) SaveUsers(db *gorm.DB) (*Users, error) {
    var err error

    // Debug a single operation, show detailed log for this operation
    err = db.Debug().Create(&u).Error
    if err != nil {
        return &Users{}, err
    }
    return u, nil
}

// GetUsers returns a user based on email
func (u *Users) GetUsers(db *gorm.DB) (*Users, error) {
    account := &Users{}
    if err := db.Debug().Table("users").Where("username = ?", u.Username).First(account).Error; err != nil {
        return nil, err
    }
    return account, nil
}

// GetAllUsers returns a list of all the user
func GetAllUsers(db *gorm.DB) (*[]Users, error) {
    users := []Users{}
    if err := db.Debug().Table("users").Find(&users).Error; err != nil {
        return &[]Users{}, err
    }
    return &users, nil
}